package com.dfc.entity;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

/**
 * @author: zsh
 * @Date:21:26 2018/5/9
 * @Description:
 */
@Data
@Entity
@Table(name = "course_resource")
@EntityListeners(AuditingEntityListener.class)
public class CourseResource {
    /**
     * 课程的id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;


    /**
     * 课程数字代码
     */
    @Column(name = "course_code",length = 6)
    private Integer courseCode;


    /**
     * 课程资源网络路径
     */
    @Column(name = "resource_net_path")
    private String resourceNetPath;


    /**
     * 课程资源服务器本地路径
     */
    @Column(name = "resource_local_path")
    private String resourceLocalPath;

    /**
     * 课程资源文件类型
     */
    @Column(name = "resource_type",length = 16)
    private String resourceType;


    /**
     * 课程资源大小
     */
    @Column(name = "resource_size",length =64)
    private String resourceSize;


    /**
     * 课程资源名
     */
    @Column(name = "resource_name",length =64)
    private String resourceName;


    /**
     * 课程资源描述
     */
    @Column(name = "resource_comment")
    private String resourceComment;


    @CreatedDate
    @Column(name = "create_time")
    private Date createTime;

    @LastModifiedDate
    @Column(name = "update_time")
    private Date updateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(Integer courseCode) {
        this.courseCode = courseCode;
    }

    public String getResourceNetPath() {
        return resourceNetPath;
    }

    public void setResourceNetPath(String resourceNetPath) {
        this.resourceNetPath = resourceNetPath;
    }

    public String getResourceLocalPath() {
        return resourceLocalPath;
    }

    public void setResourceLocalPath(String resourceLocalPath) {
        this.resourceLocalPath = resourceLocalPath;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public String getResourceSize() {
        return resourceSize;
    }

    public void setResourceSize(String resourceSize) {
        this.resourceSize = resourceSize;
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public String getResourceComment() {
        return resourceComment;
    }

    public void setResourceComment(String resourceComment) {
        this.resourceComment = resourceComment;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
