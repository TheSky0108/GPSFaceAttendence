package com.dfc.controller;


import com.dfc.entity.Result;
import com.dfc.entity.TeacherLogin;
import com.dfc.entity.User;
import com.dfc.service.TeacherLoginService;
import com.dfc.service.UserService;
import com.dfc.utils.ToDd5;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author: zsh
 * @Date:22:16 2018/5/4
 * @Description:
 */

@RestController
@RequestMapping("/teacherLogin")
public class TeacherLoginController {
    private static Logger log = LoggerFactory.getLogger(TeacherLoginController.class);

    @Autowired
    private TeacherLoginService teacherLoginService;


    @RequestMapping(value = "/getLoginCode",method = RequestMethod.GET)
    public ModelAndView getLoginCode(@RequestParam(value = "pn", defaultValue = "0") int pn) {
     ModelAndView modelAndView = new ModelAndView();
     modelAndView.setViewName("listTeacherCode");
     Pageable pageable = PageRequest.of(pn, 30);
        Page<TeacherLogin> page = teacherLoginService.getAllCodes(pageable);
        modelAndView.addObject("page", page);

        return  modelAndView;
    }
}
