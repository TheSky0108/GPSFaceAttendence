package com.dfc.service;

import com.dfc.entity.TeacherLogin;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

public interface TeacherLoginService {

    @Transactional
    Page<TeacherLogin> getAllCodes(Pageable pageable);

}
