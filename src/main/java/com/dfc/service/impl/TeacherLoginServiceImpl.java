package com.dfc.service.impl;

import com.dfc.dao.TeacherLoginDao;
import com.dfc.entity.TeacherLogin;
import com.dfc.service.TeacherLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class TeacherLoginServiceImpl implements TeacherLoginService {

    @Autowired
    private TeacherLoginDao teacherLoginDao;


    @Override
    public Page<TeacherLogin> getAllCodes(Pageable pageable) {
        return teacherLoginDao.findAll(pageable);
    }
}
