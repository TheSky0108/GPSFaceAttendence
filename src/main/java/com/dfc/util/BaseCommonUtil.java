package com.dfc.util;

import java.io.File;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class BaseCommonUtil {
    public static String uuid() {
        return UUID.randomUUID().toString();
    }


    /**
     * Title:String
     *
     * @return
     * @Description: TODO
     * @Param:
     * @Return:replace - uuid
     * @throws:
     * @author:YuMing
     * @Date:2017-12-28上午9:43:51
     */
    public static String uuidReplace() {
        return UUID.randomUUID().toString().replace("-", "");
    }


    public static String FormetFileSize(long file) {
        DecimalFormat df = new DecimalFormat("#.00");
        String fileSizeString = "";
        if (file < 1024) {
            fileSizeString = df.format((double) file) + "B";
        } else if (file < 1048576) {
            fileSizeString = df.format((double) file / 1024) + "K";
        } else if (file < 1073741824) {
            fileSizeString = df.format((double) file / 1048576) + "M";
        } else {
            fileSizeString = df.format((double) file / 1073741824) + "G";
        }
        return fileSizeString;
    }


    public static void makeSureDictoryExists(String folderPath) {
//        String folderPath = filePath.substring(0, filePath.lastIndexOf("/"));
        File folder = new File(folderPath);
        if (!folder.exists()) {
            folder.mkdirs();
        }
    }


    public static String formatDate(Date date) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd");
        return sdf.format(date);
    }


}
